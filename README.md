# Intro

This is a solution to the backend test by Revolut:
> Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts.
> 
> ...
> 
> (Full description available at Revolut)

**This readme file is incomplete without the accompanying email sent together with submission to the Revolut hiring department.**

*By Oleksii Kostyliev, April 2019*

# Disclaimer

This solution is by no means a final production-quality deliverable.

It's worth mentioning that in reality it'd be crucial to know any accepted trade-offs (read uncommitted, eventual consistency etc) for this use-case that had to be taken by the company in order to make the architecture work as needed.

This code should be viewed as an MVP that demonstrates the direction of thinking and works with limited information avaiable from the brief problem description.

Future iterations will need to account for many important factors, including but not limited to:
* real-life usage patterns;
* production transaction volumes (amount of users, accounts, transfers);
* existing SLAs (latency, availability, fault-tolerance);
* existing deployment setup (moniliths/micro-services, geo distribution etc);
* tech stack;
* ... and many more

The goal is to show how I would start solving the mentioned problem in a generic use-case with the assumptions I have to make and having a very limited timeframe for implementation.

The solution includes metric and logging services to start gathering any relevant telemetry early. In live environments those naive implementations would be replaced with smth much more production-ready.

# Assumptions

* Each account is defined in a single currency.
* Cross-account transfers with implicit currency conversion is possible.
* Not given that Revolut uses a traditional SQL RDBMS to persist accounts information. Therefore, I haven't tied the implementation to any existing in-memory transactional SQL DB, instead implemented an initial approach to handling transactions over a generic storage.
* Micro-service(-ish) architecture is assumed therefore specific bits of functionality (e.g. currency conversion service) may be remote and even unreacheable. Timeout handling is introduced to mitigate that.

# Project structure
## In a nutshell
The project is written in Kotlin and makes heavy use of Kotlin coroutines for non-blocking execution and concurrency handling. This also provides a natural built-in backpressure.

As described above, no choice for any existing embedded SQL DB was made. In live, a company may prefer a non-transactional or less strictly consistent storage for various reasons: it may guarantee better latencies and/or higher throughput, it may be easier to manage in a distrbuted setup etc.

## Tools
* Language - Kotlin 1.3.
* Build tool - Gradle 4.9 with Kotlin DSL.
* Web framework/server - Ktor 1.1.4 atop Netty.
* Concurrency - Kotlin coroutines 1.2.0 (also used by Ktor).
* Logging - Log4j2 2.11.2.
* Testing - JUnit 4.

The project is tested on Oracle JDK 1.8 (`Java(TM) SE Runtime Environment (build 1.8.0_131-b11)`) 

## Root directory layout
* `src/main/kotlin/` - Kotlin sources directory.
* `src/main/resources/` - project resources directory.
* `src/test/kotlin/` - Kotlin test sources directory.
* `build.gradle.kts` - Gradle build script with Gradle Kotlin DSL.
* other files.

## Top-level packages
* `com.revolut.transfers.handler` - contains RESTful handlers.
* `com.revolut.transfers.main` - contains executable classes to run the application standalone.
* `com.revolut.transfers.service` - contains backend logic.

## Most important classes
* `com.revolut.transfers.service.transfer.TransferService` - service that handles transfer submission, schedules them for execution and interacts with the execution/storage layer.
* `com.revolut.transfers.service.transfer.TransferEngine` - transfer execution/storage layer that provides parallel transactional handling of transfers and updates accounts' balances accordingly. This is poor man's transaction handling with some assumptions made (and documented in the class) that allows pretty much any persistence storage underneath. Any stronger transactional guarantees will require much more subtle implementation and tuning to a specific persistence layer. This can be considered for some next iteration.

Consult KDocs on the above classes for more info.

# Tests
Correct implementation of money transfer server-side logic is crucial for this assignment. The project contains 3 high-level concurrent integration tests that focus heavily on testing `TransferService` and `TransferEngine`. These tests make active use of Kotlin coroutines to try & mimic real-life highly concurrent environment.

Given the limited timeframe, other tests were identified as less important and considered for the next iteration.

# REST API
| Operation        | REST endpoint         | Method | Payload                                                      | Response                                                                                                        |
|------------------|-----------------------|--------|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| Create account   | /account              | POST   | `json: {"currency": "eur"}`                                    | - 201 if ok, "Location" header links to the new account<br />- 400 if failed                                         |
| Update balance   | /account/{id}/balance | PATCH  | `json: {"delta": "10.0985"}`                                   | - 200 if ok, response text contains the new balance<br />- 400 if failed<br />- 404 if account with given id is not found |
| Get balance      | /account/{id}/balance | GET    |                                                              | - 200 if ok, response text contains the balance<br />- 400 if failed<br />- 404 if account with given id is not found     |
| Execute transfer | /transfer             | POST   | `json {"srcAccountId": 0, "dstAccountId": 1, "amount": "0.5"}` | - 200 if ok<br />- 404 if source or dest account ids not found<br />- 500 if failed                                       |

# Running the project
The following command builds the project, executes all tests and starts up the web server at `http://localhost:<port>`:

`./gradlew clean build run --args "<port>"`, default port is 8080

Subtitute `./gradlew` with `gradlew.bat` if running on Windows.

Use Postman or any other similar tool to interact with the HTTP REST API.