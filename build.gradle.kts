import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.revolut"
version = "0.1"

plugins {
    kotlin("jvm") version "1.3.21"
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.0")
    implementation("org.hdrhistogram:HdrHistogram:2.1.11")
    implementation("io.ktor:ktor-server-core:1.1.4")
    implementation("io.ktor:ktor-server-netty:1.1.4")
    implementation("io.ktor:ktor-jackson:1.1.4")
    implementation("org.apache.logging.log4j:log4j-api:2.11.2")
    runtimeOnly("org.apache.logging.log4j:log4j-slf4j-impl:2.11.2")
    testImplementation("junit:junit:4.12")
    testImplementation("org.hamcrest:hamcrest-all:1.3")
}

application {
    mainClassName = "com.revolut.transfers.main.Main"
    
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Test> {
    testLogging {
        showStandardStreams = true
    }
}