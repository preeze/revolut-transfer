package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.currency.Currency
import com.revolut.transfers.service.currency.Currency.EUR
import com.revolut.transfers.service.currency.Currency.RMB
import com.revolut.transfers.service.currency.Currency.USD
import com.revolut.transfers.service.logging.LoggingService
import com.revolut.transfers.service.logging.TransferLifecyclePhase
import com.revolut.transfers.service.logging.TransferLifecyclePhase.EXECUTING
import com.revolut.transfers.service.logging.TransferLifecyclePhase.FAILED
import com.revolut.transfers.service.logging.TransferLifecyclePhase.FINISHED
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import org.hamcrest.Matchers
import org.junit.Assert.assertThat
import org.junit.Assert.fail
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.math.BigDecimal
import java.util.SortedMap
import java.util.TreeMap
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Creates 3 accounts and submits 2 transfers between them. One transfer is expected to fail if happens to run first due
 * to insuffiecent balance on its source account that is topped up by another transfer (that happens to run later).
 *
 * Asserts the expected balances on those 3 accounts afterwards. The assertion includes checking correct rollback of a possibly failed transfer.
 *
 * Repeats the above multiple times.
 */
@RunWith(Parameterized::class)
class TestInterleavingTransfers(spec: Map<Currency, BigDecimal>) : AbstractTransferTest(spec = spec, loggingService = StampingLoggingService()) {

    private lateinit var transfers: Map<String, Transfer>

    override suspend fun run(scope: CoroutineScope) {
        val submission = arrayOf(
                Triple(EUR, USD, eurUsd),
                Triple(USD, RMB, usdRmb)
        ).map { (src, dst, amount) ->
            Triple(account(src), account(dst), amount)
        }

        try {
            val jobs = submission.map { (src, dst, amount) ->
                scope.async {
                    try {
                        transferService.submit(src, dst, amount)
                    } catch (e: TransferStatusException) {
                        e.transfer
                    }
                }
            }

            transfers = jobs
                    .map { it.await() }
                    .associateBy { transfer -> "${transfer.source.currency}${transfer.target.currency}" }
        } catch (e: Exception) {
            fail(e.message)
        }
    }

    override fun assert() {
        fun assertBalance(c: Currency, expected: BigDecimal) {
            val actual = accountService.getBalance(account(c))
            assertThat("$c", actual, Matchers.equalTo(expected))
        }

        val eurUsdId: Long = transfers.getValue("EURUSD").id
        val usdRmbId: Long = transfers.getValue("USDRMB").id

        val snapshot = (loggingService as StampingLoggingService).snapshot()
        val eurUsdData = snapshot.getValue(eurUsdId)
        val usdRmdData = snapshot.getValue(usdRmbId)

        if (eurUsdData.getValue(EXECUTING) < usdRmdData.getValue(EXECUTING)) {
            assertThat(eurUsdData.lastKey(), Matchers.`is`(FINISHED))
            assertThat(usdRmdData.lastKey(), Matchers.`is`(FINISHED))

            assertBalance(EUR, initialBalance - eurUsd)
            assertBalance(USD, initialBalance + currencyService.convert(EUR, USD, eurUsd) - usdRmb)
            assertBalance(RMB, initialBalance + currencyService.convert(USD, RMB, usdRmb))
        } else {
            assertThat(eurUsdData.lastKey(), Matchers.`is`(FINISHED))
            assertThat(usdRmdData.lastKey(), Matchers.`is`(FAILED))

            assertBalance(EUR, initialBalance - eurUsd)
            assertBalance(USD, initialBalance + currencyService.convert(EUR, USD, eurUsd))
            assertBalance(RMB, initialBalance)
        }
    }

    private companion object {
        private val initialBalance: BigDecimal = BigDecimal.valueOf(1000)

        private val spec: Map<Currency, BigDecimal> = Currency.values().associate { it to initialBalance }

        private val eurUsd: BigDecimal = BigDecimal.valueOf(50L)

        private val usdRmb: BigDecimal = BigDecimal.valueOf(1001L)

        // JUnit4 way to run repeated test
        @Parameterized.Parameters
        @JvmStatic
        fun parameters(): Iterable<Map<Currency, BigDecimal>> = (1..300).map { spec }
    }

    private class StampingLoggingService : LoggingService {
        private val events = ConcurrentLinkedQueue<Pair<Long, TransferLifecyclePhase>>()

        override fun logTransfer(tid: Long, phase: TransferLifecyclePhase) {
            events.add(Pair(tid, phase))
        }

        override fun logException(tid: Long, e: Exception) {}

        /**
         * Converts a queue of events into a view of which transfer arrived at which phase at which position.
         * I.e. transfer id=1 arrived at [TransferLifecyclePhase.EXECUTING] 2nd in the absolute comparison with all concurrent
         * transfers, meaning there was a transfer that started [TransferLifecyclePhase.EXECUTING] earlier.
         *
         * @return a map view of all events recorded by now:
         * - key is a transfer id
         * - value is a [SortedMap] where its:
         *      - key is a lifecycle phase of the enclosing transfer
         *      - value is the 0-based absolute order of arrival at this phase throughout all unique transfer ids.
         *      The order is individual for each phase.
         */
        fun snapshot(): Map<Long, SortedMap<TransferLifecyclePhase, Int>> = events
                .groupBy({ it.second }, { it.first })
                .mapValues { (_, tids: List<Long>) -> tids.withIndex() }
                .flatMap { (phase, tids: Iterable<IndexedValue<Long>>) ->
                    tids.map { (idx, tid) -> Triple(tid, phase, idx) }
                }.groupBy({ (tid, _, _) -> tid }, { (_, phase, idx) -> phase to idx })
                .mapValues { (_, data) ->
                    data.associateByTo(TreeMap(), { it.first }, { it.second })
                }
    }
}