package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.account.Account
import com.revolut.transfers.service.account.AccountService
import com.revolut.transfers.service.currency.Currency
import com.revolut.transfers.service.currency.Currency.EUR
import com.revolut.transfers.service.currency.Currency.USD
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.isOneOf
import org.junit.Assert.assertThat
import java.math.BigDecimal

/**
 * Issues multiple concurrent identical transfers with each of them failing to update the target account - and therefore
 * having to rollback.
 *
 * Numerous concurrent readers are expected to see correct balances despite failed transfers - rollback correctness is checked.
 */
class TestRollback : AbstractTransferTest(spec = spec, accountServiceFactory = { TestAccountService(it) }) {

    override suspend fun run(scope: CoroutineScope) {
        fun getCause(e: TransferStatusException): Throwable = when (val cause = e.cause) {
            null -> throw IllegalStateException("Cannot determine cause")
            is TransferStatusException -> getCause(cause)
            else -> cause
        }

        repeat(1000) {
            scope.launch {
                try {
                    transferService.submit(account(EUR), account(USD), BigDecimal.ONE)
                } catch (e: TransferStatusException) {
                    // exception expected trying to update the USD account
                    with(getCause(e)) {
                        if (this !is UnsupportedOperationException) {
                            throw e
                        }
                        if (message != "USD") {
                            throw e
                        }
                    }
                }
            }
        }
        repeat(10000) {
            scope.launch {
                val balance = accountService.getBalance(account(EUR))
                // dirty reads of a single in-flight transfer are possible
                assertThat(balance, isOneOf(initialBalance, initialBalance - BigDecimal.ONE))
            }
        }
    }

    override fun assert() {
        // everything should have been rolled back
        spec.forEach { currency, initial ->
            assertThat("$currency", accountService.getBalance(account(currency)), equalTo(initial))
        }
    }

    private companion object {
        private val initialBalance: BigDecimal = BigDecimal.valueOf(1000)

        private val spec: Map<Currency, BigDecimal> = Currency.values().associate { it to initialBalance }
    }

    private class TestAccountService(private val delegate: AccountService) : AccountService by delegate {
        override fun updateBalance(account: Account, delta: BigDecimal): BigDecimal = when (account.currency) {
            USD -> throw UnsupportedOperationException("USD")
            else -> delegate.updateBalance(account, delta)
        }
    }
}