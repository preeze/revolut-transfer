package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.currency.Currency
import com.revolut.transfers.service.currency.Currency.EUR
import com.revolut.transfers.service.currency.Currency.USD
import com.revolut.transfers.service.metrics.MetricsService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.hamcrest.Matchers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import java.math.BigDecimal
import kotlin.coroutines.CoroutineContext

/**
 * Launches multiple concurrent identical transfers and a single repetitive reader.
 *
 * Ensures no exceptions are encountered and the reader steadily sees non-increasing source and non-decreasing destination accounts balances.
 */
class TestConcurrentTransfers : AbstractTransferTest(spec) {

    override suspend fun run(scope: CoroutineScope) {
        val src = account(EUR)
        val dst = account(USD)

        repeat(transfersCount) {
            scope.launch {
                transferService.submit(src, dst, transferAmount)
            }
        }
        scope.launch {
            var lastSrcBalance: BigDecimal? = null
            var lastDstBalance: BigDecimal? = null
            repeat(transfersCount) {
                val srcBalance = accountService.getBalance(src)
                if (lastSrcBalance != null) {
                    assertThat(srcBalance, Matchers.lessThanOrEqualTo(lastSrcBalance))
                }
                lastSrcBalance = srcBalance

                val dstBalance = accountService.getBalance(dst)
                if (lastDstBalance != null) {
                    assertThat(dstBalance, Matchers.greaterThanOrEqualTo(lastDstBalance))
                }
                lastDstBalance = dstBalance
            }
        }
    }

    override fun assert() {
        val src = account(EUR)
        val dst = account(USD)

        val totalTransferred = transferAmount * BigDecimal.valueOf(transfersCount.toLong())
        listOf(
                src to srcStartBalance - totalTransferred,
                dst to currencyService.convert(src.currency, dst.currency, totalTransferred)
        ).forEach { (account, expected) ->
            assertEquals("For ${account.currency}", expected, accountService.getBalance(account))
        }

        println("Transfer duration percentiles as measured by the ${MetricsService::class.java.name}")
        metricsService.transferDurations.outputPercentileDistribution(System.out, 1.0)
    }


    private companion object {
        private val srcStartBalance: BigDecimal = BigDecimal.valueOf(1_000_000)

        private val spec: Map<Currency, BigDecimal> = mapOf(EUR to srcStartBalance, USD to BigDecimal.ZERO)

        private val transferAmount: BigDecimal = BigDecimal.valueOf(5.5)

        private const val transfersCount = 100_000
    }
}