package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.account.AccountService
import com.revolut.transfers.service.currency.Currency
import com.revolut.transfers.service.currency.CurrencyService
import com.revolut.transfers.service.logging.LoggingService
import com.revolut.transfers.service.metrics.MetricsService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

/**
 * Abstract parent for concurrent transfer tests.
 */
abstract class AbstractTransferTest(
        private val spec: Map<Currency, BigDecimal>,
        accountServiceFactory: (AccountService) -> AccountService = { it },
        val loggingService: LoggingService = LoggingService.NOOP
) {
    val currencyService = CurrencyService()

    val metricsService = MetricsService()

    val accountService = AccountService()

    private val transferEngine = TransferEngine(accountServiceFactory(accountService), currencyService, loggingService)

    private val accounts = spec.mapValues { (currency, _) -> accountService.createAccount(currency) }

    val transferService = TransferService(transferEngine, metricsService)

    fun account(c: Currency) = accounts.getValue(c)

//    open fun accountService() = accountService

    private fun resetBalance() {
        spec.forEach { (currency, startAmount) ->
            val account = account(currency)
            accountService.updateBalance(account, -accountService.getBalance(account))
            accountService.updateBalance(account, startAmount)
        }
    }

    @Before
    fun setUp() = resetBalance()

    @Test
    fun test() {
        runBlocking(context = coroutineDispatcher) {
            run(this)
        }

        assert()
    }

    abstract suspend fun run(scope: CoroutineScope)

    abstract fun assert()

    private companion object {
        private val coroutineDispatcher = Executors.newFixedThreadPool(20).asCoroutineDispatcher()
    }
}