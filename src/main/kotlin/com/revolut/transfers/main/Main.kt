package com.revolut.transfers.main

import com.revolut.transfers.handler.account.AccountHandler
import com.revolut.transfers.handler.transfer.TransferHandler
import com.revolut.transfers.service.account.AccountService
import com.revolut.transfers.service.currency.CurrencyService
import com.revolut.transfers.service.logging.LoggingService
import com.revolut.transfers.service.metrics.MetricsService
import com.revolut.transfers.service.transfer.TransferEngine
import com.revolut.transfers.service.transfer.TransferService
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

/**
 * Main class that initializes and starts up the Ktor web application.
 */
object Main {

    private val accountService = AccountService()

    private val transferEngine = TransferEngine(accountService, CurrencyService(), LoggingService.LOG)

    private val transferService = TransferService(transferEngine, MetricsService())

    private val accountHandler = AccountHandler(accountService)

    private val transferHandler = TransferHandler(accountService, transferService)

    @JvmStatic
    fun main(args: Array<String>) {
        val server = embeddedServer(Netty, port = if (args.isNotEmpty()) args[0].toInt() else 8080) {
            install(ContentNegotiation) {
                jackson {  }
            }

            install(StatusPages) {
                accountHandler.statusPages(this)
                transferHandler.statusPages(this)

                exception<IllegalArgumentException> { cause ->
                    call.respond(status = HttpStatusCode.BadRequest, message = cause.message ?: "")
                }

                exception<Throwable> {
                    call.respond(status = HttpStatusCode.InternalServerError, message = "Server failure")
                }

            }

            routing {
                accountHandler.routing(this)
                transferHandler.routing(this)
            }
        }

        server.start(wait = true)
    }
}