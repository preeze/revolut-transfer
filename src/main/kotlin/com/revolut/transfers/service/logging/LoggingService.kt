package com.revolut.transfers.service.logging

import org.apache.logging.log4j.LogManager

/**
 * A very simple event logger for auditing purposes.
 *
 * Production-ready service likely to stream events to ELK, Kafka or the likes for storage and further analysis.
 */
interface LoggingService {

    fun logTransfer(tid: Long, phase: TransferLifecyclePhase)

    fun logException(tid: Long, e: Exception)

    companion object {
        val NOOP: LoggingService = object : LoggingService {
            override fun logTransfer(tid: Long, phase: TransferLifecyclePhase) {}

            override fun logException(tid: Long, e: Exception) {}
        }

        val LOG: LoggingService = Log4j2LoggingService()
    }
}

/**
 * Log4j2-based [LoggingService]
 */
private class Log4j2LoggingService : LoggingService {
    private val logger = LogManager.getLogger(this::class.java)

    override fun logTransfer(tid: Long, phase: TransferLifecyclePhase) {
        logger.info("[Transfer] $tid -> $phase")
    }

    override fun logException(tid: Long, e: Exception) {
        logger.error("[Transfer] {} encountered an exception", tid, e)
    }
}

/**
 * Possible phases of a transfer lifecycle
 */
enum class TransferLifecyclePhase {
    STARTED, EXECUTING, FAILED, FINISHED
}