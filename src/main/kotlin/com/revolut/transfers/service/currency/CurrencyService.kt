package com.revolut.transfers.service.currency

import java.math.BigDecimal

/**
 * This service is here mainly to create a diversity of services and mimic rich inter-service interaction in real environments.
 */
class CurrencyService {

    private val data = listOf(
            Triple(Currency.EUR, Currency.USD, BigDecimal.valueOf(1.12)),
            Triple(Currency.EUR, Currency.RMB, BigDecimal.valueOf(7.5)),
            Triple(Currency.USD, Currency.RMB, BigDecimal.valueOf(6.72))
    ).flatMap { (from, to, rate) ->
        listOf("$from$to" to rate, "$to$from" to BigDecimal.ONE / rate)
    }.toMap()

    fun convert(from: Currency, to: Currency, amount: BigDecimal): BigDecimal = when (from) {
        to -> amount
        else -> {
            val rate = data["$from$to"] ?: throw IllegalArgumentException("No rate available for pair $from/$to")
            amount * rate
        }
    }
}