package com.revolut.transfers.service.currency

enum class Currency {
    EUR, USD, RMB
}