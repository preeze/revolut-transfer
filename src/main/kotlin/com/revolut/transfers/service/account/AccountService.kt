package com.revolut.transfers.service.account

import com.revolut.transfers.service.currency.Currency
import java.math.BigDecimal
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

/**
 * Account management service.
 */
interface AccountService {
    /**
     * Finds account by id.
     *
     * @throws UnknownAccountException if the id is unknown.
     */
    fun resolveAccount(id: Int): Account

    /**
     * Creates an account with the given currency.
     */
    fun createAccount(currency: Currency): Account

    /**
     * Adds [delta] to [account]'s balance.
     *
     * @throws InsufficientBalanceException if adding [delta] brings this account's balance below 0.
     */
    fun updateBalance(account: Account, delta: BigDecimal): BigDecimal

    /**
     * Retrieves [account]'s balance.
     */
    fun getBalance(account: Account): BigDecimal
}

/**
 * For tests.
 */
@Suppress("FunctionName")
fun AccountService(): AccountService = DefaultAccountService()

/**
 * [ConcurrentHashMap]-based implementation of [AccountService]
 */
class DefaultAccountService : AccountService {

    private val accounts = ConcurrentHashMap<Int, Account>()

    private val balances = ConcurrentHashMap<Account, BigDecimal>()

    override fun resolveAccount(id: Int): Account = when(val account = accounts[id]) {
        null -> throw UnknownAccountException(id)
        else -> account
    }

    override fun createAccount(currency: Currency): Account = with(Account(nextId(), currency)) {
        accounts[id] = this
        balances[this] = BigDecimal.ZERO
        return this
    }

    override fun updateBalance(account: Account, delta: BigDecimal): BigDecimal = balances.compute(account) { _, v ->
        if (v == null) {
            throw IllegalStateException("Unknown account: $account")
        }
        val result = v + delta
        when (result < BigDecimal.ZERO) {
            true -> throw InsufficientBalanceException(account, v, -delta)
            else -> result
        }
    }!!

    override fun getBalance(account: Account): BigDecimal = balances.compute(account) { _, v ->
        v ?: throw IllegalStateException("Unknown account: $account")
    }!!

    companion object {
        private val id = AtomicInteger()

        private fun nextId() = id.getAndIncrement()
    }
}
