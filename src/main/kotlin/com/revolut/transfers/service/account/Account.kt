package com.revolut.transfers.service.account

import com.revolut.transfers.service.currency.Currency

data class Account(val id: Int, val currency: Currency)