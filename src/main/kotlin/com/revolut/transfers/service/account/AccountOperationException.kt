package com.revolut.transfers.service.account

import java.math.BigDecimal

sealed class AccountOperationException(message: String) : Exception(message)

class InsufficientBalanceException(account: Account, available: BigDecimal, withdrawal: BigDecimal) :
        AccountOperationException("Account: $account, available: $available, attempted: $withdrawal")

class UnknownAccountException(id: Int) : AccountOperationException("Unknown account id: $id")