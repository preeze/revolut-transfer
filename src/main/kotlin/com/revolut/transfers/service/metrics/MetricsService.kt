package com.revolut.transfers.service.metrics

import org.HdrHistogram.ConcurrentHistogram

/**
 * A very simple implementation of a metrics collection service.
 *
 * Only supports recording transfer durations backed by HdrHistograms.
 *
 * A real implementation will most likely be based on smth like Dropwizard and will report regularly to Graphite/Datadog/etc.
 *
 */
class MetricsService {

    // visible for tests
    val transferDurations: ConcurrentHistogram = ConcurrentHistogram(2)

    fun recordTransfer(millis: Long) {
        transferDurations.recordValue(millis)
    }
}