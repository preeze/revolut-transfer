package com.revolut.transfers.service.transfer

sealed class TransferStatusException(val transfer: Transfer, cause: Throwable) : Exception("Transfer: $transfer", cause)

class TransferFailedException(transfer: Transfer, cause: Throwable) : TransferStatusException(transfer, cause)

class TransferIncompleteException(transfer: Transfer, cause: Throwable) : TransferStatusException(transfer, cause)