package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.account.Account
import com.revolut.transfers.service.metrics.MetricsService
import dispatcher
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.math.BigDecimal
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

/**
 * A service that orchestrates transfers execution. It accepts clients' requests and dispatches them down to [TransferEngine].
 *
 * This service maintains a bounded queue of transfer requests that allows for backpressure shall transfers execution slow down.
 *
 * Additionally it provides initial metrics support.
 */
class TransferService(private val transferEngine: TransferEngine, private val metricsService: MetricsService) {

    // channel size can be monitored
    private val channel = Channel<Transfer>(queueSize)

    private val results = ConcurrentHashMap<Transfer, CompletableDeferred<Unit>>()

    private val id: AtomicLong = AtomicLong()

    init {
        scheduleTransfers()
    }

    fun submit(from: Account, to: Account, amount: BigDecimal): Transfer {
        val request = Transfer(transferId(), from, to, amount)
        runBlocking(context = dispatcher) {
            channel.send(request)
            results.computeIfAbsent(request) { CompletableDeferred() }.await()
        }
        results -= request
        return request
    }

    private fun scheduleTransfers() {
        repeat(processors) {
            CoroutineScope(context = dispatcher).launch {
                for (request in channel) {
                    val job = results.computeIfAbsent(request) { CompletableDeferred() }
                    try {
                        execute(request)
                        job.complete(Unit)
                    } catch (e: Exception) {
                        job.completeExceptionally(e)
                    }
                }
            }
        }
    }

    private suspend fun execute(request: Transfer) {
        val start = System.currentTimeMillis()
        try {
            transferEngine.execute(request)
        } finally {
            metricsService.recordTransfer(System.currentTimeMillis() - start)
        }
    }

    private fun transferId(): Long = id.getAndIncrement()

    companion object {
        private const val queueSize = 10_000

        private const val processors = 50
    }
}