import kotlinx.coroutines.asCoroutineDispatcher
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

/**
 * A coroutines dispatcher used to execute all transfers.
 */
val dispatcher: CoroutineContext = Executors.newFixedThreadPool(10).asCoroutineDispatcher()