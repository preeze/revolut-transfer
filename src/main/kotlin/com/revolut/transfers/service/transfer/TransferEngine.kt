package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.account.Account
import com.revolut.transfers.service.account.AccountService
import com.revolut.transfers.service.account.InsufficientBalanceException
import com.revolut.transfers.service.currency.CurrencyService
import com.revolut.transfers.service.logging.LoggingService
import com.revolut.transfers.service.logging.TransferLifecyclePhase.*
import dispatcher
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import java.math.BigDecimal
import java.util.Deque
import java.util.LinkedList
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

/**
 * A transfer execution and persistence backend.
 *
 * For consistency guarantees no 2 transfers for the same account will execute in parallel.
 *
 * Provides 1-staged transactional semantics with rollback on errors. Account reads may see uncommited transfers - assumed
 * not to be a problem as there can only be max 1 transfer running at a time for each given account.
 *
 * The implementation mimics communicating with remote services (currency conversion, account mgmt) and therefore accounts
 * for network delays or even downtimes.
 */
class TransferEngine(
        private val accountService: AccountService,
        private val currencyService: CurrencyService,
        private val loggingService: LoggingService
) {

    private val locks = ConcurrentHashMap.newKeySet<Account>()

    suspend fun execute(request: Transfer) = try {
        loggingService.logTransfer(request.id, STARTED)
        executeTransaction(request)
        loggingService.logTransfer(request.id, FINISHED)
    } catch (e: Exception) {
        loggingService.logTransfer(request.id, FAILED)
        throw when (e) {
            is TransferStatusException -> e
            else -> TransferIncompleteException(request, e)
        }
    }

    private suspend fun executeTransaction(request: Transfer) {
        val source = request.source
        val target = request.target

        val sourceAmount = request.amount
        val targetAmount = if (source.currency == target.currency) {
            sourceAmount
        } else {
            try {
                withTimeout { currencyService.convert(source.currency, target.currency, sourceAmount) }
            } catch (e: Exception) {
                loggingService.logException(request.id, e)
                throw TransferFailedException(request, e)
            }
        }

        val accounts = mapOf(source to -sourceAmount, target to targetAmount)
        executeTransaction(request, accounts)
    }

    private suspend fun updateBalance(account: Account, spec: Map<Account, BigDecimal>) {
        val delta = spec.getValue(account)
        withTimeout {
            accountService.updateBalance(account, delta)
        }
    }

    private suspend fun executeTransaction(request: Transfer, spec: Map<Account, BigDecimal>) {
        val accounts = spec.keys

        for (account in accounts) {
            while (!locks.add(account)) {
                delay(100)
            }
        }

        try {
            loggingService.logTransfer(request.id, EXECUTING)
            val processed: Deque<Account> = LinkedList()
            try {
                for (account in accounts) {
                    processed.addFirst(account)
                    updateBalance(account, spec)
                }
            } catch (e: Exception) {
                val exceptions = LinkedList<Exception>()
                if (!canRollback(e)) {
                    processed.removeFirst()
                    exceptions += e
                }

                val rollbackSpec = spec.mapValues { (_, v) -> -v }
                for (account in processed) {
                    try {
                        updateBalance(account, rollbackSpec)
                    } catch (e: Exception) {
                        exceptions += e
                    }
                }

                if (exceptions.isEmpty()) {
                    loggingService.logException(request.id, e)
                    throw TransferFailedException(request, e)
                } else {
                    exceptions.forEach { exception ->
                        loggingService.logException(request.id, exception)
                    }
                    throw TransferIncompleteException(request, exceptions.first)
                }
            }
        } finally {
            for (account in accounts) {
                locks.remove(account)
            }
        }
    }

    private suspend fun <T> withTimeout(block: () -> T) = withContext(context = dispatcher) {
        val job = async { block() }
        withTimeout(timeMillis = TimeUnit.SECONDS.toMillis(10)) {
            job.await()
        }
    }

    /*
     * Will be more sophisticated for some real storage.
     */
    private fun canRollback(e: Exception) = when (e) {
        is CancellationException -> false
        is InsufficientBalanceException -> false
        else -> true
    }
}