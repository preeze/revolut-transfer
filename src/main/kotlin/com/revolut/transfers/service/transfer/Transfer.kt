package com.revolut.transfers.service.transfer

import com.revolut.transfers.service.account.Account
import java.math.BigDecimal

data class Transfer(
        val id: Long,
        val source: Account,
        val target: Account,
        /**
         * Amount in the source currency.
         */
        val amount: BigDecimal
) {
    init {
        require(source != target) { "Source and target need to be different" }
        require(amount > BigDecimal.ZERO) { "Amount needs to be positive" }
    }
}