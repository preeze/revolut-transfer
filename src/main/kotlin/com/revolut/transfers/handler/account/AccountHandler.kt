package com.revolut.transfers.handler.account

import com.revolut.transfers.handler.AbstractHandler
import com.revolut.transfers.service.account.AccountService
import com.revolut.transfers.service.account.InsufficientBalanceException
import com.revolut.transfers.service.account.UnknownAccountException
import com.revolut.transfers.service.currency.Currency
import io.ktor.application.call
import io.ktor.features.StatusPages
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.accept
import io.ktor.routing.get
import io.ktor.routing.patch
import io.ktor.routing.post
import java.math.BigDecimal

/**
 * HTTP handler for account operations
 */
class AccountHandler(private val accountService: AccountService) : AbstractHandler {
    override fun routing(routing: Routing) = with(routing) {
        accept(ContentType.Application.Json) {
            post("/account") {
                val request = call.receive<Map<String, String>>()
                val id = createAccount(request.getValue("currency"))
                call.response.headers.append(HttpHeaders.Location, "/account/$id")
                call.respondText(status = HttpStatusCode.Created, text = "OK")
            }
        }

        get("/account/{id}/balance") {
            val id = call.parameters["id"]!!.toInt()
            val balance = getBalance(id)
            call.respondText(text = balance.toPlainString())
        }

        accept(ContentType.Application.Json) {
            patch("/account/{id}/balance") {
                val id = call.parameters["id"]!!.toInt()
                val request = call.receive<Map<String, String>>()
                val balance = updateBalance(id, BigDecimal(request.getValue("delta")))
                call.respondText(text = balance.toPlainString())
            }
        }
    }

    override fun statusPages(configuration: StatusPages.Configuration) = with(configuration) {
        exception<UnknownAccountException> { cause ->
            call.respond(status = HttpStatusCode.NotFound, message = cause.message ?: "")
        }

        exception<InsufficientBalanceException> { cause ->
            call.respond(status = HttpStatusCode.BadRequest, message = cause.message ?: "")
        }
    }

    private fun createAccount(currency: String): Int {
        val c = try {
            Currency.valueOf(currency.toUpperCase())
        } catch (e: Exception) {
            throw IllegalArgumentException("Invalid currency code: $currency")
        }
        return accountService.createAccount(c).id
    }

    private fun updateBalance(id: Int, amount: BigDecimal): BigDecimal = with(accountService) {
        updateBalance(resolveAccount(id), amount)
    }

    private fun getBalance(id: Int): BigDecimal = with(accountService) {
        getBalance(resolveAccount(id))
    }
}