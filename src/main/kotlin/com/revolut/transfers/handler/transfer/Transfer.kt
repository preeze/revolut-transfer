package com.revolut.transfers.handler.transfer

import java.math.BigDecimal

data class Transfer(val srcAccountId: Int, val dstAccountId: Int, val amount: BigDecimal)