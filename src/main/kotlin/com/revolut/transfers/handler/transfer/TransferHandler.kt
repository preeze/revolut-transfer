package com.revolut.transfers.handler.transfer

import com.revolut.transfers.handler.AbstractHandler
import com.revolut.transfers.service.account.AccountService
import com.revolut.transfers.service.transfer.TransferService
import com.revolut.transfers.service.transfer.TransferStatusException
import io.ktor.application.call
import io.ktor.features.StatusPages
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.accept
import io.ktor.routing.post

/**
 * HTTP handler for transfer operations.
 */
class TransferHandler(
        private val accountService: AccountService,
        private val transferService: TransferService
) : AbstractHandler {
    override fun routing(routing: Routing): Route = with(routing) {
        accept(ContentType.Application.Json) {
            post("/transfer") {
                val request = call.receive<Transfer>()
                submitTransfer(request)
                call.respondText(text = "OK")
            }
        }
    }

    override fun statusPages(configuration: StatusPages.Configuration) = with(configuration) {
        exception<TransferStatusException> {
            call.respondText(status = HttpStatusCode.InternalServerError, text = "Transfer failed")
        }
    }

    private fun submitTransfer(transfer: Transfer) = with(transfer) {
        transferService.submit(
                from = accountService.resolveAccount(srcAccountId),
                to = accountService.resolveAccount(dstAccountId),
                amount = amount
        )
    }
}