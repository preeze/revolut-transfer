package com.revolut.transfers.handler

import io.ktor.features.StatusPages
import io.ktor.routing.Route
import io.ktor.routing.Routing

/**
 * Abstract HTTP handler. Provides extension points to configure routing and error handling.
 */
interface AbstractHandler {
    /**
     * Add routing info to this Ktor's application routing ([Routing]).
     */
    fun routing(routing: Routing): Route

    /**
     * Add error handling to this Ktor's application error handling.
      */
    fun statusPages(configuration: StatusPages.Configuration) {}
}